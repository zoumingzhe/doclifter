# Makefile for the doclifter project
VERSION=$(shell sed <doclifter -n -e '/^version *= *"\(.*\)"/s//\1/p')

PREFIX=/usr
MANDIR=$(PREFIX)/share/man/man1
BINDIR=$(PREFIX)/bin

DOCS    = README COPYING TODO PATCHES \
		doclifter.xml doclifter.1 manlifter.xml manlifter.1
SOURCES = doclifter manlifter Makefile $(DOCS) tests/ doclifter-logo.png

all: doclifter-$(VERSION).tar.gz

install: doclifter.1
	cp doclifter $(BINDIR)
	gzip <doclifter.1 >$(MANDIR)/doclifter.1.gz
	rm doclifter.1

doclifter.1: doclifter.xml
	xmlto man doclifter.xml

manlifter.1: manlifter.xml
	xmlto man manlifter.xml

doclifter.html: doclifter.xml
	xmlto xhtml-nochunks doclifter.xml

manlifter.html: manlifter.xml
	xmlto xhtml-nochunks manlifter.xml

doclifter-$(VERSION).tar.gz: $(SOURCES)
	mkdir doclifter-$(VERSION)
	cp -r $(SOURCES) doclifter-$(VERSION)
	tar -czf doclifter-$(VERSION).tar.gz doclifter-$(VERSION)
	rm -fr doclifter-$(VERSION)
	ls -l doclifter-$(VERSION).tar.gz

doclifter-$(VERSION).md5: doclifter-$(VERSION).tar.gz
	@md5sum doclifter-$(VERSION).tar.gz >doclifter-$(VERSION).md5

# Note: This will show a souriious diff id pic2plot is not installed.
check:
	@cd tests >/dev/null; make --quiet

pylint:
	@pylint --score=n doclifter

pychecker:
	@echo "Expect 4 warnings."
	@ln -f doclifter doclifter.py
	@-pychecker --only --quiet --limit 50 doclifter.py
	@rm -f doclifter.py doclifter.pyc

dist: doclifter-$(VERSION).tar.gz

clean:
	rm -f doclifter.html manlifter.html doclifter.1 manlifter.1
	rm -f *.pyc docliftertest.xml foobar* fixed* *~ bugs.html
	rm -f index.html *.tar.gz *.md5 *old

release: doclifter-$(VERSION).tar.gz doclifter-$(VERSION).md5 doclifter.html manlifter.html
	shipper version=$(VERSION) | sh -e -x

htmlclean:
	rm *.html
refresh: htmlclean doclifter.html manlifter.html
	shipper -N -w version=$(VERSION) | sh -e -x

# This is used only for updating the bugs page on my website.
# It won't work for anyone else.
REMOTE=esr@login.ibiblio.org
UPDIR=/public/html/catb/esr/doclifter
update:
	problemgen.py >bugs.html
	ssh $(REMOTE) rm -fr $(UPDIR)/prepatch
	scp -r bugs.html prepatch/ $(REMOTE):$(UPDIR)
